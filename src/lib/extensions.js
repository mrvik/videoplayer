const video = ext => ({ ext, isVideo: true })
const image = ext => ({ ext, isVideo: false })

export const extensions = [
    ...["mp4", "mkv", "mov"].map(video),
    ...["jpg", "jpeg", "webp", "png", "svg"].map(image),
]

export const supportedExtensions = new Set(extensions.map(({ ext }) => ext))
