import { FluentBundle, FluentResource } from "@fluent/bundle"
import { negotiateLanguages } from "@fluent/langneg"

const locales = import.meta.glob("./locales/*.ftl", { as: "raw" })
const prepareLanguage = (name, target) => {
    const bundle = new FluentBundle(name)

    return async () => {
        const content = await target().then(res => new FluentResource(res))
        bundle.addResource(content)

        return bundle
    }
}

export const languages = Object.fromEntries(
    Object.entries(locales)
        .map(([locale, target]) => [locale.replace(/^.*\/([^/.]+)\.ftl$/, "$1"), target])
        .map(([locale, target]) => [locale, prepareLanguage(locale, target)])
)

export const supportedLanguages = Object.keys(languages)
export const defaultLocale = "es"

export const negotiate = userLocales =>
    Promise.all(
        negotiateLanguages(userLocales, supportedLanguages, {
            defaultLocale,
            strategy: "lookup",
        }).map(lang => languages[lang]())
    )
