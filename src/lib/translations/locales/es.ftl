open-window = Abrir ventana de vídeo
open-directory = Abrir carpeta
clear-playlist = Limpiar lista de reproducción
loading-playlist = Cargando lista de reproducción
playlist-elements = {NUMBER($count) ->
    [1] Un elemento
    *[other] {$count} elementos
} multimedia en la lista de reproducción
configure-player = Configurar ventana de vídeo
player-not-open = La ventana del reproductor no está abierta
    .instructions = Haz click en el botón {open-window} y después en {configure-player} en la ventana que se abrirá
playlist = Lista de reproducción
