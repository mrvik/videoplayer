open-window = Open window
open-directory = Open directory
clear-playlist = Clear playlist
loading-playlist = Loading playlist
playlist-elements = {NUMBER($count) ->
    [1] One multimedia element
    *[other] {$count} multimedia elements
} in playlist
configure-player = Configure video player
player-not-open = Player window is not open
    .instructions = Click the {open-window} button and then click {configure-player} in the new window
playlist = Playlist
