export const ssr = false
export const prerender = false

export const load = async () => {
    self.opener.postMessage({id: "ready"})
}
