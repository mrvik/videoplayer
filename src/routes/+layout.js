export const ssr = false
export const prerender = false

export const load = () => {
    const userLanguages = self.navigator.languages

    return {
        userLanguages,
    }
}
