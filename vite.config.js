import { sveltekit } from "@sveltejs/kit/vite"
import { defineConfig } from "vitest/config"
import basicSSL from "@vitejs/plugin-basic-ssl"
import svelteFluent from "@nubolab-ffwd/svelte-fluent/rollup-plugin"

export default defineConfig({
    plugins: [svelteFluent(), sveltekit(), basicSSL()],
    test: {
        include: ["src/**/*.{test,spec}.{js,ts}"],
    },
})
